(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Created by Rafael Teles <rmteles@translucentcomputing.com> on 20-Feb-17.
 */
/**
 * Created by Rafael Teles <rmteles@translucentcomputing.com> on 20-Feb-17.
 */
(function (exports) {
    "use strict";

    if (!exports.report) exports.report = {};
    exports.report.getScoutedResources = function () {
        return {
            wood: getResource("[title = Madeira]"),
            stone: getResource("[title = Argila]"),
            iron: getResource("[title = Ferro]")
        }
    };

    function getResource(cssSelector) {
        var resourceTxt = $($(cssSelector)[0]).parent().text();
        resourceTxt = resourceTxt.replace(".", "");
        return Number(resourceTxt);
    }

})(module.exports);
},{}],2:[function(require,module,exports){
/**
 * Created by Rafael Teles <rmteles@translucentcomputing.com> on 20-Feb-17.
 */
(function (exports) {
    "use strict";

    // A.prototype.valor = function ();
    // a = 3;

    exports.unity = {
        spearFighter: {
            haul: 25
        },
        swordsman: {
            haul: 15
        },
        axeman: {
            haul: 10
        },
        archer: {
            haul: 10
        },
        scout: {
            haul: 0
        },
        lightCavalary: {
            haul: 80
        },
        mounterArcher: {
            haul: 50
        },
        heavyCavalary: {
            haul: 50
        },
        ram: {
            haul: 0
        },
        catapult: {
            haul: 0
        },
        paladin: {
            haul: 100
        },
        nobleman: {
            haul: 0
        },
        militia: {
            haul: 0
        }
    };

})(module.exports);
},{}],3:[function(require,module,exports){
/**
 * Created by Rafael Teles <rmteles@translucentcomputing.com> on 20-Feb-17.
 */

(function () {
    "use strict";

    var Unity = require("../../common/unity.js").unity;
    var resourcesScouted = require("../../common/report/resourcesScouted.js");
    console.log(Unity);

    var scoutedResources = resourcesScouted.report.getScoutedResources();
    var total = scoutedResources.iron + scoutedResources.wood + scoutedResources.stone;

    var resultStr = "";
    for (var key in Unity) {
        if (Unity.hasOwnProperty(key) && Unity[key].haul > 0) {
            resultStr += key + " -> " + Math.ceil(total / Unity[key].haul) + "\n";
        }
    }

    alert(resultStr);

})();
},{"../../common/report/resourcesScouted.js":1,"../../common/unity.js":2}]},{},[3])