/**
 * Created by Rafael Teles <rmteles@translucentcomputing.com> on 20-Feb-17.
 */
var gulp = require('gulp');
var concat = require('gulp-concat');
var clean = require('gulp-rimraf');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var notify = require('gulp-notify');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var argv = require('yargs').argv;
var browserify = require('gulp-browserify');

var emptyFn = function () {};

gulp.task('default', [
    'reportDiscoveredResourcesTroopsCalculator'
], emptyFn);

gulp.task('reportDiscoveredResourcesTroopsCalculator', [], function () {
    return gulp.src("scripts/reportDiscoveredResourcesTroopsCalculator/reportResourceExtractor.js")
        .pipe(browserify({}))
        .pipe(gulp.dest('./dist'))
});

