/**
 * Created by Rafael Teles <rmteles@translucentcomputing.com> on 20-Feb-17.
 */
/**
 * Created by Rafael Teles <rmteles@translucentcomputing.com> on 20-Feb-17.
 */
(function (exports) {
    "use strict";

    if (!exports.report) exports.report = {};
    exports.report.getScoutedResources = function () {
        return {
            wood: getResource("[title = Madeira]"),
            stone: getResource("[title = Argila]"),
            iron: getResource("[title = Ferro]")
        }
    };

    function getResource(cssSelector) {
        var resourceTxt = $($(cssSelector)[0]).parent().text();
        resourceTxt = resourceTxt.replace(".", "");
        return Number(resourceTxt);
    }

})(module.exports);