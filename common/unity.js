/**
 * Created by Rafael Teles <rmteles@translucentcomputing.com> on 20-Feb-17.
 */
(function (exports) {
    "use strict";

    // A.prototype.valor = function ();
    // a = 3;

    exports.unity = {
        spearFighter: {
            haul: 25
        },
        swordsman: {
            haul: 15
        },
        axeman: {
            haul: 10
        },
        archer: {
            haul: 10
        },
        scout: {
            haul: 0
        },
        lightCavalary: {
            haul: 80
        },
        mounterArcher: {
            haul: 50
        },
        heavyCavalary: {
            haul: 50
        },
        ram: {
            haul: 0
        },
        catapult: {
            haul: 0
        },
        paladin: {
            haul: 100
        },
        nobleman: {
            haul: 0
        },
        militia: {
            haul: 0
        }
    };

})(module.exports);